// A $( document ).ready() block.
$( document ).ready(function() {
    
    const sectionNavButton = $('.presentation-nav > span, .floating-nav--button');

    sectionNavButton.on('click', function(){
        console.log('click', $(this))
    })

    sectionNavButton.click(function() {
        const linked_section = $(this).data("linked-section");

        $([document.documentElement, document.body]).animate({
            scrollTop: $(`div[data-section="${linked_section}"]`).offset().top
        }, 500);
    });


    const percent = 40;
    let window_scrolled;

    $(window).scroll(function() {
        window_scrolled = (percent * $(document).height()) / 100;

        if ($(window).scrollTop() + $(window).height() >= window_scrolled) {
           $('.floating-nav').css('opacity', 1);
        }else{
            $('.floating-nav').css('opacity', 0);
        }
    });


    
});


var tag = document.createElement('script');
tag.id = 'iframe-demo';
tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('existing-iframe-example', {
        playerVars: { 
            'autoplay': 0,
            'controls': 0, 
            'rel' : 0,
            'fs' : 0,
            'loop': 0,
            'rel': 0
         },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        },
    });
    console.log('ready')
    window.player = player;
}

function onPlayerReady(event) {
    document.getElementById('existing-iframe-example').style.borderColor = '#FF6D00';
}

function changeBorderColor(playerStatus) {
    var color;
    if (playerStatus == -1) {
    color = "#37474F"; // unstarted = gray
    } else if (playerStatus == 0) {
    color = "#FFFF00"; // ended = yellow
    } else if (playerStatus == 1) {
    color = "#33691E"; // playing = green
    } else if (playerStatus == 2) {
    color = "#DD2C00"; // paused = red
    } else if (playerStatus == 3) {
    color = "#AA00FF"; // buffering = purple
    } else if (playerStatus == 5) {
    color = "#FF6DOO"; // video cued = orange
    }
    if (color) {
    document.getElementById('existing-iframe-example').style.borderColor = color;
    }
}
function onPlayerStateChange(event) {
    changeBorderColor(event.data);
}